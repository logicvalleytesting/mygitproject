package com.kmms.optimize;

import java.util.concurrent.TimeUnit;

//import org.testng.Assert;
//import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
//import com.Utils.Excel_Utilities;

//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

public class Login extends basic {
	
	
	//private static final TimeUnit SECONDS = null;

	@Test(priority=0)
	public static void Optimise_Login() throws Exception
	{
		try
		{
	   // driver.manage().timeouts().pageLoadTimeout(500, SECONDS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.findElement(Obj_Rep.getLocator("Officer_ID")).sendKeys(base.getProperty("Officer_ID"));
		driver.findElement(Obj_Rep.getLocator("Password")).sendKeys(base.getProperty("Password"));
		driver.findElement(Obj_Rep.getLocator("Login_button")).click();
		Thread.sleep(7000);
		System.out.print("Login successful"+ "\n" +"");
		
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
//	@Test(priority=1,dataProvider = "login_data")
//	public void login_Validate(String UserId,String Password,String Msg) throws Exception
//	{
//	try
//		{
//		
//		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
//		driver.findElement(Obj_Rep.getLocator("Officer_ID")).sendKeys(UserId);
//		driver.findElement(Obj_Rep.getLocator("Password")).sendKeys(Password);
//		driver.findElement(Obj_Rep.getLocator("Login_button")).click();
//		Thread.sleep(5000);
//		//WebDriverWait wait = new WebDriverWait(driver, waitTime);
//		//wait.until(ExpectedConditions.presenceOfElementLocated(alertmsg)); 
//		String actualErrorDisplayed = driver.findElement(Obj_Rep.getLocator("alertmsg")).getText();
//		System.out.println("Actual: " + actualErrorDisplayed);
//		String requiredErrorMessage = Msg;
//		System.out.println("Expected: " + requiredErrorMessage);
//		Assert.assertEquals(requiredErrorMessage, actualErrorDisplayed);
//		Thread.sleep(5000);
//		
//		
//		}
//		catch(Exception e)
//		{
//			System.out.println(e.getMessage());
//			e.printStackTrace();
//		}
//	}
//	
//	@DataProvider(name="login_data")
//	public Object[][] loginData() throws Exception {
//		System.out.print("Login Validation Starts"+ "\n" +"");
//		Object[][] arrayObject = Excel_Utilities.getExcelData(base.getProperty("DataSheet_Path"),"Login");
//		System.out.print("Login Validation End"+ "\n" +"");
//		return arrayObject;
//	}

//	public static void main(String args[]) throws Exception
//	{
//	  Search os = new Search();
//	  os.optimise_search(); 
//	}
	
	
}
