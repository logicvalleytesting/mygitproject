package com.kmms.optimize;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;



public class ADM extends basic {
WebElement element;
	
	@Test(priority=1) 
	
	public void ADM_Dashboard() throws Exception
	{
		try {
			Thread.sleep(12000);

			driver.findElement(Obj_Rep.getLocator("Menu")).click();
			Thread.sleep(2000);
			System.out.println("Below are the menu available for");
			String ADMOfficer = driver.findElement(Obj_Rep.getLocator("Officer_ID")).getText();
	        System.out.println("" + ADMOfficer);
			Thread.sleep(2000);
			
			if(driver.getPageSource().contains("Send Notification to Officers")){
				System.out.println("Send Notification to Officers");
				}else{
				System.out.println("Send Notification to Officers menu is missing");
				}
			
			if(driver.getPageSource().contains("Officer Activity Status")){
				System.out.println("Officer Activity Status");
				}else{
				System.out.println("Officer Activity Status menu is missing");
				}
			
			if(driver.getPageSource().contains("Flash news")){
				System.out.println("Flash news");
				}else{
				System.out.println("Flash news menu is missing");
				}
			
			if(driver.getPageSource().contains("Pending Actions")){
				System.out.println("Pending Actions");
				}else{
				System.out.println("Pending Actions menu is missing");
				}
			
			if(driver.getPageSource().contains("Search case actions")){
				System.out.println("Search case actions");
				}else{
				System.out.println("Search case actions menu is not displayed");
				}
			
			if(driver.getPageSource().contains("Reports")){
				System.out.println("Reports");
				}else{
				System.out.println("Reports menu is not displayed");
				}
			
			if(driver.getPageSource().contains("Case Upload")){
				System.out.println("Case Upload");
				}else{
				System.out.println("Case Upload menu is not displayed");
				}
			
			if(driver.getPageSource().contains("Clamped Image Search")){
				System.out.println("Clamped Image Search");
				}else{
				System.out.println("Clamped Image Search menu is not displayed");
				}
			
			if(driver.getPageSource().contains("Bailed Image Search")){
				System.out.println("Bailed Image Search");
				}else{
				System.out.println("Bailed Image Search menu is not displayed");
				}
			
			if(driver.getPageSource().contains("Manage Case Return")){
				System.out.println("Manage Case Return");
				}else{
				System.out.println("Manage Case Return menu is not displayed");
				}
			
			if(driver.getPageSource().contains("ANPR-VRM FileUpload")){
				System.out.println("ANPR-VRM FileUpload");
				}else{
				System.out.println("ANPR-VRM FileUpload menu is not displayed");
				}
			
			if(driver.getPageSource().contains("VRM Image Search")){
				System.out.println("VRM Image Search");
				}else{
				System.out.println("VRM Image Search menu is not displayed");
				}
		} 
		catch (Exception e) {
			System.out.println("Error in finding menu"+ "\n" +"");
			e.printStackTrace();
		}

}
}




