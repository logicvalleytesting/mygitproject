package com.kmms.optimize;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
//import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import com.repo.Basic_Details;
import com.repo.Obj_Repository;

public class basic {
	public static WebDriver driver;
	public static Obj_Repository Obj_Rep; 
	public static Basic_Details base;
	public static String project_path;
	
@Parameters("browser")
@BeforeSuite
		public WebDriver open_browser(String browser)
		{
	    	try
	    	{
	    		project_path = System.getProperty("user.dir");
	    		base = new Basic_Details();
	    		Basic_Details.Open_File(project_path+"\\src\\main\\java\\com\\Utils\\BasicDetails.properties");
	    	    Obj_Rep = new Obj_Repository (project_path+"\\src\\main\\java\\com\\Utils\\ObjectMap.properties");
	
			switch (browser)
			{
			case "Chrome":
			{
			System.setProperty("webdriver.chrome.driver","C:\\Users\\SriVidhya\\git\\MyLocalGit\\MavenProject\\New Driver\\chromedriver.exe");	
			System.out.print("Browser is Launched"+ "\n" +"");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.navigate().to(base.getProperty("URL"));
			String title = driver.getTitle();
			System.out.println(title);
//			String expectedTitle = "Login";
//		    String actualTitle = driver.getTitle();
//		    Assert.assertEquals(actualTitle, expectedTitle);
			System.out.print("URL is opened"+ "\n" +"");
			
			break;
			}
			}
			
	    	}
	    	catch(Exception e)
	    	{
	    		System.out.println("Error in opening the application"+ "\n" +"");
				e.printStackTrace();
				System.exit(1);
	    	}
	    	return driver;
		}


		
//@AfterSuite
//		public void close_browser()
//		{
//		driver.quit();
//		}

}
