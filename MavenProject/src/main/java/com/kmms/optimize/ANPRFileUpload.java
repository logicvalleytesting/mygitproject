package com.kmms.optimize;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class ANPRFileUpload extends basic {
	
	WebElement element;
	
	@Test(priority=11) 
	
	public void ANPRfile_Upload() throws Exception
	{
		try {
			Thread.sleep(12000);
			driver.findElement(Obj_Rep.getLocator("Menu")).click();
			Thread.sleep(2000);
			System.out.println("ANPR file is upload start"+ "\n" +"");
			Thread.sleep(2000);
			driver.findElement(Obj_Rep.getLocator("anprfileupload")).click();
			Thread.sleep(5000);
			element = driver.findElement(Obj_Rep.getLocator("uploadfile"));
			Thread.sleep(5000);
			element.sendKeys("D:\\demo\\Distress\\to upload small size.csv");
			Thread.sleep(5000);
			driver.findElement(Obj_Rep.getLocator("uploadfileok")).click();
			Thread.sleep(7000);
			driver.findElement(Obj_Rep.getLocator("ok")).click();
			Thread.sleep(2000);
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			Date date = new Date();
			String date1= dateFormat.format(date);
            System.out.println("ANPR file uploaded date and time is " +date1);
			System.out.println("ANPR file is uploaded successfully"+ "\n" +"");
			

		} 
		catch (Exception e) {
			System.out.println("Error in File upload"+ "\n" +"");
			e.printStackTrace();
		}
}
		

}
