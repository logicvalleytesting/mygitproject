package com.kmms.optimize;

import java.util.List;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


public class Dashboard extends basic {
	
	WebElement element;
	
	@Test(priority = 1)
	public void Optimise_CaseWidget() throws Exception {
		
		CaseAction();
		Thread.sleep(2000);
	}
	
	@Test(priority = 2)
	public void Optimise_warrantWidget() throws Exception {
		
		Warrant_Matching();
		Thread.sleep(2000);
	
	}
	
	@Test(priority = 3)
	public void Optimise_RankingWidget() throws Exception {
		
		Ranking();
		Thread.sleep(2000);
	
	}
	
//	@Test(priority = 4)
//	public void Optimise_LocationWidget() throws Exception {
//		
//		LastKnown_Location();
//		Thread.sleep(2000);
//	
//	}
	
	
	 public void CaseAction() {
	      try {
	    	  
	    	Thread.sleep(12000);
		    driver.findElement(Obj_Rep.getLocator("CAdatepic_id")).click();
		    
//		    Thread.sleep(2000);
//		    driver.findElement(Obj_Rep.getLocator("TxtFromDT_id")).clear();
//		    
//		    Thread.sleep(2000);
//		    driver.findElement(Obj_Rep.getLocator("TxtFromDT_id")).sendKeys("09/01/2018");
//		    
//		    Thread.sleep(2000);
//		    System.out.println(Obj_Rep.getLocator("Srcdate"));
//		    
//		    Thread.sleep(2000);
//		    driver.findElement(Obj_Rep.getLocator("TxtFromDT_id")).sendKeys(Keys.ENTER);
//		    
		    Thread.sleep(2000);
		    driver.findElement(Obj_Rep.getLocator("BtnCAok_id")).click();
	    	
	        Thread.sleep(2000);
	        System.out.println("Case Action Widget"+ "\n" +"");
	        String CApaidcnt=driver.findElement(Obj_Rep.getLocator("CApaidcntid")).getAttribute("innerHTML");
	        System.out.println("paid count :"+CApaidcnt);
	        
	        String CAppcnt=driver.findElement(Obj_Rep.getLocator("CApartpaidcntid")).getAttribute("innerHTML");
	        System.out.println("Part paid count :"+CAppcnt);
	        
	        String CAreturncnt=driver.findElement(Obj_Rep.getLocator("CArtncntid")).getAttribute("innerHTML");
	        System.out.println("Returned count :"+CAreturncnt);
	        
	        String CAllcnt=driver.findElement(Obj_Rep.getLocator("CALlcntid")).getAttribute("innerHTML");
	        System.out.println("Left Letter count :"+CAllcnt);
	        
	        String CAdefcnt=driver.findElement(Obj_Rep.getLocator("CAdefcntid")).getAttribute("innerHTML");
	        System.out.println("Defendant contact count :"+CAdefcnt);
	        
	        String CAclampcnt=driver.findElement(Obj_Rep.getLocator("CAclampcntid")).getAttribute("innerHTML");
	        System.out.println("Clamped count :"+CAclampcnt);
	        
	        driver.findElement(Obj_Rep.getLocator("Changeclass1")).click();
	      
	        driver.findElement(Obj_Rep.getLocator("fancybox_btn_id")).click();
	  
	        driver.findElement(Obj_Rep.getLocator("fancybox_btn_id1")).click();
	      }
	        catch (Exception e) {
				System.out.println("Error in Case Action Widget"+ "\n" +"");
				e.printStackTrace();
			}
	}
	 
	 public void Warrant_Matching()
		{
			 try {
			 Thread.sleep(12000);

			 System.out.println("Warrant Matching Widget"+ "\n" +"");  
			 
			 driver.findElement(Obj_Rep.getLocator("WMdticid")).click();
			 Thread.sleep(2000);

//			 driver.findElement(Obj_Rep.getLocator("WMtxtfrmdtid")).clear();
//			 Thread.sleep(2000);
//			 
//			 driver.findElement(Obj_Rep.getLocator("WMtxtfrmdtid")).sendKeys("09/01/2018");
//			 Thread.sleep(2000);
//			 
//			 driver.findElement(Obj_Rep.getLocator("WMtxtfrmdtid")).sendKeys(Keys.ENTER);
//			 Thread.sleep(2000);
			 
			 driver.findElement(Obj_Rep.getLocator("WMdtokbtn")).click();
			 Thread.sleep(2000);
			 
			 WebElement Divtable = driver.findElement(By.id("tblWMA"));
			 List<WebElement> rows = Divtable.findElements(By.tagName("tr"));
			 List<WebElement> column = Divtable.findElements(By.tagName("td"));
			 List<String> value = new ArrayList<String>();
			 System.out.println(rows.size());
			 for (int j=0; j<rows.size(); j++){
			 value.add(rows.get(j).getText());
			 System.out.println(rows.get(j).getText());
			 }
			 
			} 
			catch (Exception e) 
			{
				System.out.println("Error in Warrant Matching"+ "\n" +"");
				e.printStackTrace();
			}
		}
       
	  public void Ranking() {
	      try {
	    	  
		    Thread.sleep(12000);
		    System.out.println("Ranking Paid Widget"+ "\n" +"");  
		    
	        driver.findElement(Obj_Rep.getLocator("Rdt_id")).click();
	        
	        Thread.sleep(2000);
	        driver.findElement(Obj_Rep.getLocator("txtfrmdt_id")).click();
	        
//	        Thread.sleep(2000);
//	        driver.findElement(Obj_Rep.getLocator("txtfrmdt_id")).clear();
//	        
//	        Thread.sleep(2000);
//	        driver.findElement(Obj_Rep.getLocator("txtfrmdt_id")).sendKeys("09/01/2018");
//	        
//	        Thread.sleep(2000);
//	        driver.findElement(Obj_Rep.getLocator("txtfrmdt_id")).sendKeys(Keys.ENTER);
//	        
	        Thread.sleep(2000);
	        driver.findElement(Obj_Rep.getLocator("BtnRanok")).click();
	        
	        Thread.sleep(2000);
	        driver.findElement(Obj_Rep.getLocator("RanPaid_id")).click();
	      
	        WebElement Divtable = driver.findElement(By.id("tblRA"));
	        List<WebElement> rows = Divtable.findElements(By.tagName("tr"));
	        List<WebElement> column = Divtable.findElements(By.tagName("td"));
	        List<String> value = new ArrayList<String>();
	        
	        System.out.println(rows.size());
	        for (int j=0; j<rows.size(); j++)
	        {
	        value.add(rows.get(j).getText());
	        System.out.println(rows.get(j).getText());
	        } 
	     
	        Thread.sleep(2000);
	        System.out.println("\n");
	        System.out.println("Ranking Returned Widget"+ "\n" +""); 
	        
	        
	         driver.findElement(Obj_Rep.getLocator("Ranrtn_id")).click();
	         Thread.sleep(2000);
	       
	         WebElement Divrtntbl = driver.findElement(By.id("tblbodyReturned"));
	         List<WebElement> Rtnrows = Divrtntbl.findElements(By.tagName("tr"));
	         List<WebElement> Rtncolumn = Divrtntbl.findElements(By.tagName("td"));
	         List<String> Rtnval = new ArrayList<String>();
	            
	         System.out.println(Rtnrows.size());
	         for (int j=0; j<Rtnrows.size(); j++){
	         Rtnval.add(Rtnrows.get(j).getText());
	         System.out.println(Rtnrows.get(j).getText());
	          }
	         System.out.println("\n");
	         System.out.println("Ranking Part paid Widget"+ "\n" +""); 
	         
	         driver.findElement(Obj_Rep.getLocator("Ran_PP_id")).click();
	         Thread.sleep(2000);
	         
	         WebElement Divpptbl = driver.findElement(By.id("tblbodyPP"));
	         List<WebElement> pprows = Divpptbl.findElements(By.tagName("tr"));
	         List<WebElement> ppcolumn = Divpptbl.findElements(By.tagName("td"));
	         List<String> ppval = new ArrayList<String>();
	            
	          System.out.println(Rtnrows.size());
	          for (int j=0; j<pprows.size(); j++){
	          ppval.add(pprows.get(j).getText());
	          System.out.println(pprows.get(j).getText());
	          }
	         
	        
	      } 
			catch (Exception e) 
			{
				System.out.println("Error in Ranking"+ "\n" +"");
				e.printStackTrace();
			}
	
	      }
	  
//	  public void LastKnown_Location() {
//	      try {
//
//		      Thread.sleep(12000);
//
//	          driver.findElement(Obj_Rep.getLocator("LstLdt")).click();
//	           
//	          Thread.sleep(2000);
//		      driver.findElement(Obj_Rep.getLocator("Txfrmlo_id")).clear();
//	          
//	          Thread.sleep(2000);
//		      driver.findElement(Obj_Rep.getLocator("Txfrmlo_id")).sendKeys("09/01/2018");
//	          
//		      Thread.sleep(2000);
//		      driver.findElement(Obj_Rep.getLocator("Txfrmlo_id")).sendKeys(Keys.ENTER);
//	           
//		      Thread.sleep(2000);
//	          driver.findElement(Obj_Rep.getLocator("Btnlodtok_id")).click();
//	              
//	          Thread.sleep(2000);
//	          driver.findElement(Obj_Rep.getLocator("ALastLocation")).click();
//	         
//	          Thread.sleep(2000);
//	          driver.findElement(Obj_Rep.getLocator("ACloseLocation")).click();      
//	           
//	          Thread.sleep(2000);
//	          //driver.findElement(Obj_Rep.getLocator("Txtsrcoff_id")).sendKeys(Obj_Rep.getProperty("Txtlocid"));
//	        
//	          Thread.sleep(2000);
//	          driver.findElement(Obj_Rep.getLocator("Seloff_id5626")).click(); 
//	          
//	          Thread.sleep(2000);
//	          driver.findElement(Obj_Rep.getLocator("Alstloc_id")).click();  
//	          
//	          Thread.sleep(2000);
//	          driver.findElement(Obj_Rep.getLocator("ACloseLocation")).click();    
//	          
//	          Thread.sleep(2000);
//	          driver.findElement(Obj_Rep.getLocator("optimg_id")).click();  
//	      
//	   }
//
//		catch (Exception e) 
//		{
//			System.out.println("Error in Last Known Location"+ "\n" +"");
//			e.printStackTrace();
//		}

//    }
	    	  
}

