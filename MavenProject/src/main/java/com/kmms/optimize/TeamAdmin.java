package com.kmms.optimize;

//import static org.testng.Assert.assertEquals;
//import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;
//import org.openqa.selenium.By;
//import org.apache.bcel.generic.Select;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class TeamAdmin extends basic {
	
	WebElement element;

	@Test(priority = 6)
	public void Optimise_MenuNavigation() throws Exception {
		
		navigation_code();
		Thread.sleep(2000);
	}
	
	@Test(priority = 7)
	public void Optimise_AddOfficer() throws Exception {
		
		Add_officer();
		Thread.sleep(2000);
	
	}
	
	
//	@Test(priority = 8)
//	public void view_role() throws Exception {
//		
//		view_officer();
//		Thread.sleep(1000);
//	}
	
	
	
	public void Add_officer() {
		try {
			
			Thread.sleep(4000);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(Obj_Rep.getLocator("offid")).sendKeys("2001");

			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			Select se = new Select(driver.findElement(Obj_Rep.getLocator("manager")));
			se.selectByVisibleText("Brian LaMee");
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Select se1 = new Select(driver.findElement(Obj_Rep.getLocator("adm")));
			se1.selectByVisibleText("Rose Vandenouer");
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Select se2 = new Select(driver.findElement(Obj_Rep.getLocator("comp")));
			se2.selectByVisibleText("Marston");
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Select se3 = new Select(driver.findElement(Obj_Rep.getLocator("typ")));
			se3.selectByVisibleText("PAYE");
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Select se4 = new Select(driver.findElement(Obj_Rep.getLocator("Acceptactn")));
			se4.selectByVisibleText("Yes");
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Select se5 = new Select(driver.findElement(Obj_Rep.getLocator("offrole")));
			se5.selectByVisibleText("Attending Officer");
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(Obj_Rep.getLocator("email")).sendKeys("aaa@gmail.com");
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(Obj_Rep.getLocator("offname")).sendKeys("Joy");
			
//			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
//			driver.findElement(Obj_Rep.getLocator("pass")).sendKeys("1111");
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Select se6 = new Select(driver.findElement(Obj_Rep.getLocator("role")));
			se6.selectByVisibleText("Officer");
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Select se7 = new Select(driver.findElement(Obj_Rep.getLocator("Certificated")));
			se7.selectByVisibleText("Yes");
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Select se8 = new Select(driver.findElement(Obj_Rep.getLocator("Teamleader")));
			se8.selectByVisibleText("Yes");
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(Obj_Rep.getLocator("Mob")).sendKeys("11111111111");
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.findElement(Obj_Rep.getLocator("addbtn")).click();
			
//			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
//			driver.findElement(Obj_Rep.getLocator("alertboxclse")).click();
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS); 
			System.out.println("Officer is created successfully"+ "\n" +"");
			
		}
		catch (Exception e) 
		{
			System.out.println("Error in Add Code"+ "\n" +"");
			e.printStackTrace();
		}
	}
	
	public void navigation_code()
	{
		try 
		{
			Thread.sleep(3000);

			element = driver.findElement(Obj_Rep.getLocator("Menu"));
			element.click();
			Thread.sleep(5000);
			element = driver.findElement(Obj_Rep.getLocator("TeamAdmin"));
			element.click();
			Thread.sleep(5000);
			element = driver.findElement(Obj_Rep.getLocator("createbtn"));
			element.click();
			Thread.sleep(5000);
			
		} 
		catch (Exception e) 
		{
			System.out.println("Error in List Navigation"+ "\n" +"");
			e.printStackTrace();
		}
	}
	

//	public void view_officer()
//	{
//		try 
//		{
//			Thread.sleep(1000);
//			element = driver.findElement(Obj_Rep.getLocator("officerid"));
//			String officerid = element.getText();
//			System.out.println("Officer ID: " + officerid);
//			Assert.assertEquals(officerid, "Officer ID");
//			Thread.sleep(1000);
//			
//			element = driver.findElement(Obj_Rep.getLocator("officername"));
//			String officername = element.getText();
//			System.out.println("Officer Name: " + officername);
//			Assert.assertEquals(officername, "Officer Name");
//			Thread.sleep(1000);
//			
//			element = driver.findElement(Obj_Rep.getLocator("admname"));
//			String admname = element.getText();
//			System.out.println("ADM Name: " + admname);
//			Assert.assertEquals(admname, "ADM Name");
//			Thread.sleep(1000);
//			
//			element = driver.findElement(Obj_Rep.getLocator("managername"));
//			String managername = element.getText();
//			System.out.println("Manager Name: " + managername);
//			Assert.assertEquals(managername, "Manager Name");
//			Thread.sleep(1000);
//			
//			element = driver.findElement(Obj_Rep.getLocator("registereddate"));
//			String registereddate = element.getText();
//			System.out.println("Registered Date: " + registereddate);
//			Assert.assertEquals(registereddate, "Registered Date");
//			Thread.sleep(1000);
//			
//			element = driver.findElement(Obj_Rep.getLocator("emailid"));
//			String emailid = element.getText();
//			System.out.println("Email: " + emailid);
//			Assert.assertEquals(emailid, "Email");
//			Thread.sleep(1000);
//			
//			element = driver.findElement(Obj_Rep.getLocator("mobile"));
//			String mobile = element.getText();
//			System.out.println("Mobile: " + mobile);
//			Assert.assertEquals(mobile, "Mobile");
//			Thread.sleep(1000);
//
//		} 
//		
//		catch (Exception e) 
//		{
//			System.out.println("Error in Edit Role");
//			e.printStackTrace();
//		}
//	}
//			

}
