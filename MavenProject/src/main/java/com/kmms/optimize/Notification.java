package com.kmms.optimize;


import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Notification extends basic {
	
	WebElement element;
	
	@Test(priority=5) 
	
	public void Officer_Notification() throws Exception
	{
		try {
			Thread.sleep(12000);
			driver.findElement(Obj_Rep.getLocator("Menu")).click();
			Thread.sleep(2000);
			driver.findElement(Obj_Rep.getLocator("Notification")).click();
			Thread.sleep(5000);
			driver.findElement(Obj_Rep.getLocator("selectofficer")).click();
			Thread.sleep(2000);
			driver.findElement(Obj_Rep.getLocator("of")).click();
			Thread.sleep(3000);
			driver.findElement(Obj_Rep.getLocator("txtarea")).click();
			Thread.sleep(2000);
			driver.findElement(Obj_Rep.getLocator("txtarea")).sendKeys("Test");
			Thread.sleep(2000);
			driver.findElement(Obj_Rep.getLocator("sendbtn")).click();
			Thread.sleep(2000);
			driver.findElement(Obj_Rep.getLocator("notifyclose")).click();
			System.out.println("Notification is sent"+ "\n" +"");
			
		} 
		catch (Exception e) {
			System.out.println("Error in Sending Notification"+ "\n" +"");
			e.printStackTrace();
		}
}
		

}
