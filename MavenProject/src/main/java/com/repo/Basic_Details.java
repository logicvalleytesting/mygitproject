package com.repo;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Basic_Details {
	
	static Properties properties;
	
	public static void Open_File(String filepath)
	{
		properties = new Properties();
		try 
	      {
	      FileInputStream fis = new FileInputStream(filepath);
	      properties.load(fis);
	      fis.close();
	      }
	      catch (IOException e)
	      {
	            System.out.println(e.getMessage());
	      }
	}
	public String getProperty(String ElementName) throws Exception {
		try
		{
        //Read value using the logical name as Key
        String locator = properties.getProperty(ElementName);
        return locator;
		}
		catch (Exception e)
		{
			System.err.format("Error message: " +e);
        	  
          }
		return null;
        }

}
