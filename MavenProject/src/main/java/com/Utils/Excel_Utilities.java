package com.Utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_Utilities 
{
	     static FileInputStream XlsxFileToRead = null;
		 static XSSFWorkbook workbook = null;
		 static XSSFSheet sheet = null;
		 static XSSFRow row;
		 static XSSFCell cell;
		 public static int RowNum;
		 public static int ColNum;
		 public static String[][] tabArray = null;
		 public static int ci;
		 public static int cj;
		@SuppressWarnings("deprecation")
		public static Object[][] getExcelData(String fileName,String Sheetname) //throws Exception
		{
			try 
			{
				XlsxFileToRead = new FileInputStream(fileName);
				workbook = new XSSFWorkbook(XlsxFileToRead);
				sheet = workbook.getSheet(Sheetname);
				RowNum = sheet.getLastRowNum();
				System.out.println("Last Row Number is  "+RowNum+ "\n" +"");
				Row r = sheet.getRow(RowNum);
				ColNum=  r.getLastCellNum();
				System.out.println("Last Column Number is  "+ColNum+ "\n" +"");
				tabArray=new String[RowNum][ColNum];
				 
				   ci=0;
	 
				   for (int i=1;i<=RowNum;i++, ci++)
				   {           	   
					  cj=0;
					   for (int j=0;j<ColNum;j++, cj++)
					   {
						   System.out.println("Value of i "+i);
						   System.out.println("Value of j "+j);
						   System.out.println(""+ "\n" +"");
						   //tabArray[ci][cj]=getCellData(i,j);
						   cell = sheet.getRow(i).getCell(j);
						    if (cell.getCellType() == Cell.CELL_TYPE_STRING)
						   {
			                	tabArray[ci][cj] = cell.getStringCellValue();
			                	System.out.print("Value read is "+tabArray[ci][cj]+ "\n" +"");
			                } 
						    else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
							   {
				                	tabArray[ci][cj] = String.valueOf(cell.getNumericCellValue());
				                	System.out.print("Value read is "+tabArray[ci][cj]+ "\n" +"");
				                } 
			                
					   else if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
					   {
			                
			                    tabArray[ci][cj] = null;
			                    System.out.print("Value read is "+tabArray[ci][cj]+ "\n" +"");
			                    continue;
			                }
						 
						
	 
							}
	 
						}
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
				//System.exit(1);
			} 
			catch (IOException e)
			{
				e.printStackTrace();
				//System.exit(1);
			}
			catch (NullPointerException e)
			{
				e.printStackTrace();
				//System.exit(1);
			}
			return tabArray;
		}
}
		

